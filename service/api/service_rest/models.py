from django.db import models


class Technician(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=200, unique=True)

    def __str__(self):
        first_name = self.first_name
        last_name = self.last_name
        name = first_name + last_name
        return name

    class Meta:
        ordering = ("first_name", "last_name", "employee_id")


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=200, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("vin", "sold")



class Appointment(models.Model):
    date_time = models.CharField(max_length=200)
    reason = models.CharField(max_length=200)
    status = models.CharField(max_length=200, default='created')
    vin = models.CharField(max_length=200)
    customer = models.CharField(max_length=200)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE
    )

    def __str__(self):
        return f"{self.customer} with {self.technician}"

    def cancel(self):
        self.status = 'canceled'
        self.save()

    def finish(self):
        self.status = 'finished'
        self.save()

    class Meta:
        ordering = (
            "date_time",
            "reason",
            "vin",
            "status",
            "customer",
            "technician",
            )
