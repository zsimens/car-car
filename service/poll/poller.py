import django
import os
import sys
import time
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

from service_rest.models import AutomobileVO


def get_auto():
    response = requests.get("http://project-beta-inventory-api-1:8000/api/automobiles/")
    content = response.json()
    print(content)

    while True:
        print('Service poller polling for data')
        try:
            for auto in content["autos"]:
                AutomobileVO.objects.update_or_create(
                    import_href=auto["href"],
                    vin=auto["vin"],
                    defaults={"sold": auto["sold"]},
                )
                print(auto)

        except Exception as e:
            print(e, file=sys.stderr)

        time.sleep(60)


if __name__ == "__main__":
    get_auto()
