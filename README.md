# CarCar

Team:

Zachary Simens

Since he worked alone, he chose to work on the service microservice.

## Setting up and Running the Project (and API Documentation)

To get this website off the ground, it must be built up with three docker commands:
1.
```
docker volume create beta-data
```
this command creates a database for use by the other microservice containers

2.
```
docker-compose build
```
this command builds all of the necessary containers for the project

3.
```
docker-compose up
```
this command spins up the containers and gets them talking to each other via the poller

This website, CarCar, is an application which manages the inventory, service centre, and sales of a car dealership. Its functioning relies on a poller, which is a microservice that continually updates the inventory api service every 60 seconds. The inventory app stores up-to-date information on automobiles, vehicle models, manufacturers, and technicians. (Please see the note below about the sales microservice.)

The inventory can be accessed from the navbar options on the browser homepage; functionality, along with the request types and links that the options take, are as follows:

For creating, updating, deleting, and listing manufacturers, use the navbar options, which execute the following requests to carry out the commands which follow each request:

GET http://localhost:8100/api/manufacturers/
list manufacturers

POST http://localhost:8100/api/manufacturers/
create a new manufacturer

GET http://localhost:8100/api/manufacturers/:id/
get a specific manufacturer

PUT http://localhost:8100/api/manufacturers/:id/
update a specific manufacturer

DELETE http://localhost:8100/api/manufacturers/:id/
delete a specific manufacturer

For creating, updating, deleting, and listing vehicle models, use the navbar options, which execute the following requests to carry out the commands which follow each request:

GET http://localhost:8100/api/models/
list vehicle models

POST http://localhost:8100/api/models/
create a new vehicle model

GET http://localhost:8100/api/models/:id/
get a specific model

PUT http://localhost:8100/api/models/:id/
update a specific model

DELETE http://localhost:8100/api/models/:id/
delete a specific model


For creating, updating, deleting, and listing vehicle models, use the navbar options, which execute the following requests to carry out the commands which follow each request:

GET http://localhost:8100/api/models/
list automobiles

POST http://localhost:8100/api/models/
create a new automobile

GET http://localhost:8100/api/automobiles/:vin/
get a specific automobile

PUT http://localhost:8100/api/automobiles/:vin/
update a specific automobile

DELETE http://localhost:8100/api/automobiles/:vin/
delete a specific automobile



The inventory app stores up-to-date information on automobiles, vehicle models, manufacturers, and technicians. (Please see the note below about the sales microservice.)

The service api functions are also presented as options on the browser homepage; their functionality is listed below.
, along with the request types and links that the options take, are as follows:

For creating, deleting, and listing technicians, use the navbar options, which execute the following requests to carry out the commands which follow each request:

GET http://localhost:8080/api/technicians/
list technicians

POST http://localhost:8080/api/technicians/
create a new technician

DELETE http://localhost:8080/api/technicians/:id/
list technicians

For creating, updating, deleting, and listing appointments, use the navbar options, which execute the following requests to carry out the commands which follow each request:

GET http://localhost:8080/api/appointments/
list appointments

POST http://localhost:8080/api/appointments/
create a new appointment

DELETE http://localhost:8080/api/appointments/:id/
delete an appointment

PUT http://localhost:8080/api/appointments/:id/
update an appointment's status to 'canceled'

PUT http://localhost:8080/api/appointments/:id/
update an appointment's status to 'finished'


## Design (and Documentation)

![Project Beta Diagramme](./Images/Project-Beta-Readme-Diagramme.png)

The website home page is based on front-end modules of the project, which are handled by React. Eacho a Javascript file which corresponds to the display of a user-friendly browser interface that accepts typed and selected input in various fields and then, upon the click of the page function buttons, routes that input as a JSON request to the pertinent microservice. The back end of this project consists of these microservices. Each microservice contains Python models, each of which is instantiated via the JSON request argument which, when initiated by the client browser, is passed into the appropriate Python microservice RESTful API views function. The output of the view function, upon execution, is in JSON as well. That output is then routed through the appropriate url pathway to the corresponding front-end module. The React library and the module display the accessed content on the screen.

## Inventory microservice
This application has an API that maintain the automobile inventory. It has a Manufacturer model, which creates instances of manufacturing companies whose brands are serviced and sold at the dealership. The Vehicle model creates an instance of a certain model made by each manufacturer. Finally, the Automobile Model creates profile instances of an actual car, including the manufacturing companies and their vehicle models.

## Service microservice

Explain your models and integration with the inventory
microservice, here.

Models:
Technician: used to create or update an instance of a technician when listing all technicians, adding or deleting a single technician, or accessing a technician in order to create an appointment.

AutomobileVO: used by the poller to create, read, or update, or delete any automobile in the inventory. The automobile poller automatically updates the AutomobileVO every 60 seconds.

Appointment: used to create, read, update, or delete both lists of appointments, and to change their status. This model contains data_time, reason, status, vin, customer, and technician fields; technician is a foreign key to the Technician model.


The API list view functions either list automobiles in the inventory or technicians who are employed, or they list all scheduled appointments automobiles, along with the technician assigned to those appointment and information about the client, date, time, car, etc. These list view functions also provide the functionality for the pages on the website having to do with creating digital profiles of single automobiles, technicians, or appointments

The API 'show' view functions carry out the tasks of each of the pages associated with looking up, deleting, and updating single appointment, car, or technician profiles.

Finally, buttons on the Appointments List page

## Sales microservice

(Note: since Zach worked alone, the sales microservice has not been included.)
Explain your models and integration with the inventory
microservice, here.
