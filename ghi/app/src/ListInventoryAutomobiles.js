import React, { useEffect, useState } from 'react';

function ListInventoryAutomobiles() {
    const [inventoryAutomobiles, setInventoryAutomobiles] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/');

        if (response.ok) {
            const data = await response.json();
            console.log(data);
            setInventoryAutomobiles(data.autos);
        }
    }

    useEffect(()=>{
        getData();
    }, []);

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Manufacturer</th>
                </tr>
            </thead>
            <tbody>
                {inventoryAutomobiles.map(automobile => {
                    return (
                        <tr key={automobile.name}>
                            <td>{ automobile.color }</td>
                            <td>{ automobile.year }</td>
                            <td>{ automobile.vin }</td>
                            <td>{ automobile.sold }</td>
                            <td>{ automobile.model_id }</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default ListInventoryAutomobiles;
