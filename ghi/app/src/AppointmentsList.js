import React, { useEffect, useState } from 'react';

function AppointmentsList() {
    const [appointments, setAppointments] = useState([]);
    const [automobiles, setAutomobiles] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');

        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments);
        }
    };

    const getAutomobileData = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/');
        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos);
        }
    };

    const cancelAppointment = async (id) => {
        const metadata = {
            method: "put",
            headers: {"Content-Type": "application/json"}
        };
        const response = await fetch(`http://localhost:8080/api/appointments/${id}/cancel/`, metadata);
        if (response.ok) {
            getData();
        }
    };

    const finishAppointment = async (id) => {
        const metadata = {
            method: "put",
            headers: {"Content-Type": "application/json"},
        };
        const response = await fetch(`http://localhost:8080/api/appointments/${id}/finish/`, metadata);
        if (response.ok) {
            getData();
        }
    };

    useEffect(()=>{
        getData();
        getAutomobileData();
    }, []);

    return (
        <div>
            <h1>Appointments</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Date and Time</th>
                        <th>Reason</th>
                        <th>Status</th>
                        <th>VIN</th>
                        <th>Customer</th>
                        <th>Technician</th>
                        <th>Finish</th>
                        <th>Cancel</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map(appointment => {
                        return (
                            <>
                            {appointment.status === "created" ?
                            <tr key={appointment.id}>
                                <td>{ appointment.date_time }</td>
                                <td>{ appointment.reason }</td>
                                <td>{ appointment.status }</td>
                                <td>{ appointment.vin }</td>
                                <td>{ appointment.customer }</td>
                                <td>{ appointment.technician.first_name }</td>
                                <td><button onClick={() => finishAppointment(appointment.id)}>Finish</button></td>
                                <td><button onClick={() => cancelAppointment(appointment.id)}>Cancel</button></td>
                            </tr>
                            : ""}
                            </>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default AppointmentsList;
