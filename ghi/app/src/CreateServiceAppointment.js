import React, { useState, useEffect } from 'react';

function CreateServiceAppointment() {
    const [technicians, setTechnicians] = useState([]);
    const [formData, setFormData] = useState({
        date_time: '',
        reason: '',
        vin: '',
        customer: '',
        technician: '',
    });

    const getData = async () => {
        const technicianUrl = 'http://localhost:8080/api/technicians/';
        const response = await fetch(technicianUrl);
        console.log(JSON.stringify(response));

        if (response.ok) {
            const data = await response.json();
            console.log(data);
            setTechnicians(data.technicians);
        }
    };

    useEffect(()=>{
        getData();
    }, []);

    const handleSubmit = async (e) => {
        e.preventDefault();

        const url = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                date_time: '',
                reason: '',
                vin: '',
                customer: '',
                technician: '',
            })
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,

            [inputName]: value
        });
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create A New Appointment</h1>
                    <form onSubmit={handleSubmit} id="create-appointment-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.date_time} placeholder="Date_Time" required type="text" name="date_time" id="date_time" className="form-control" />
                            <label htmlFor="date_time">DateTime</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.reason} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" />
                            <label htmlFor="reason">Reason</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.vin} placeholder="Vin" required type="text" name="vin" id="vin" className="form-control" />
                            <label htmlFor="vin">VIN</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.customer} placeholder="Customer" required type="customer" name="customer" id="customer" className="form-control" />
                            <label htmlFor="customer">Customer</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleFormChange} value={formData.technician} required name="technician" id="technician" className="form-select">
                                <option value="">Choose a technician</option>
                                {technicians.map(technician => {
                                    return (
                                        <option key={technician.id} value={technician.id}>{`${technician.first_name} ${technician.last_name}`}</option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create Appointment</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default CreateServiceAppointment;
