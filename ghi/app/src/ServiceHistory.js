import React, { useEffect, useState } from 'react';

function ServiceHistory() {
    const [automobiles, setAutomobiles] = useState([]);
    const [appointments, setAppointments] = useState([]);
    const [formData, setFormData] = useState([]);

    const getAppointmentData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments');
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments);
        }
    };

    const getAutomobileData = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles')
        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos);
        }
    }

    useEffect(()=>{
        getAppointmentData();
        getAutomobileData();
    }, [])

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,

            [inputName]: value
        });
    }

    return (
        <div>
            <h1>Appointment History</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Date and Time</th>
                        <th>Reason</th>
                        <th>Status</th>
                        <th>VIN</th>
                        <th>Customer</th>
                        <th>Technician</th>
                        <th>VIP?</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map(appointment =>{
                        return (
                            <>
                            {appointment.status !== "created" ?
                            <tr key={appointment.id}>
                                <td>{ appointment.date_time }</td>
                                <td>{ appointment.reason }</td>
                                <td>{ appointment.status }</td>
                                <td>{ appointment.vin }</td>
                                <td>{ appointment.customer }</td>
                                <td>{ appointment.technician.first_name }</td>
                                <td>{ automobiles.map (automobile => {
                                    return (
                                        <>
                                        {automobile.sold === "true" ?
                                        <tr key={automobile.vin}>
                                            <td>{ automobile.vip = 'true'}</td>
                                        </tr>
                                        : ""}
                                        </>
                                    );
                                })}</td>
                            </tr>
                            : ""}
                            </>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )

}

export default ServiceHistory;
