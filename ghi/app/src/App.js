import React, { BrowserRouter, Routes, Route } from 'react-router-dom';
import CreateAutomobile from './CreateAutomobile';
import CreateManufacturer from './CreateManufacturer';
import CreateVehicleModel from './CreateVehicleModel';
import CreateTechnician from './CreateTechnician';
import CreateServiceAppointment from './CreateServiceAppointment';
import ListInventoryAutomobiles from './ListInventoryAutomobiles';
import MainPage from './MainPage';
import ManufacturersList from './ManufacturersList';
import TechniciansList from './TechniciansList';
import VehicleModels from './VehicleModels';
import AppointmentsList from './AppointmentsList';
import ServiceHistory from './ServiceHistory';
import Nav from './Nav';

function App() {

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/createautomobile" element={<CreateAutomobile />} />
          <Route path="/createmanufacturer" element={<CreateManufacturer />} />
          <Route path="/createvehiclemodel" element={<CreateVehicleModel />} />
          <Route path="/createtechnician" element={<CreateTechnician />} />
          <Route path="/createappointment" element={<CreateServiceAppointment />} />
          <Route path="/listinventoryautomobiles" element={<ListInventoryAutomobiles />} />
          <Route path="/technicians" element={<TechniciansList />} />
          <Route path="/listmanufacturers" element={<ManufacturersList />} />
          <Route path="/listvehiclemodels" element={<VehicleModels />} />
          <Route path="/listappointments" element={<AppointmentsList />} />
          <Route path="/listappointmenthistory" element={<ServiceHistory />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
